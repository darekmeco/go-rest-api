package main

import (
	"github.com/cnetic.pl/restapi/server"
)

func main() {
	server.Run()
}
