package routes

import (
	app "github.com/cnetic.pl/restapi/app/controllers"
	blogRoutes "github.com/cnetic.pl/restapi/app/modules/blog/routes"
	users "github.com/cnetic.pl/restapi/app/modules/users/controllers"
	"github.com/cnetic.pl/restapi/middlewares"
	"github.com/gin-gonic/gin"
	"github.com/swaggo/files"
	"github.com/swaggo/gin-swagger"
	//_ "rest/first-api/docs"
)

//SetupRouter ... Configure routes
func SetupRouter() *gin.Engine {
	r := gin.Default()
	blogRoutes.SetupRouter(r)

	authRoutes := r.Group("/auth")
	{
		authRoutes.POST("register", app.Register)
		authRoutes.POST("/login", middlewares.AuthMiddleware().LoginHandler)
	}
	mainRoutes := r.Group("/")
	{
		mainRoutes.GET("", app.Index)
	}
	userRoutes := r.Group("/user")
	{
		userRoutes.Use(middlewares.AuthMiddleware().MiddlewareFunc())
		{
			userRoutes.GET("", users.GetUsers)
			userRoutes.POST("", users.CreateUser)
			userRoutes.PATCH(":user_id", users.UpdateUser)
			userRoutes.DELETE(":user_id", users.DeleteUser)
		}
	}
	swaggerRoutes := r.Group("/swagger")
	{
		url := ginSwagger.URL("http://localhost:8080/swagger/doc.json")
		swaggerRoutes.GET("*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
	}



	return r
}
