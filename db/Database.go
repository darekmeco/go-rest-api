package db

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"os"
	"strconv"
)

var DB *gorm.DB

// DBConfig represents db configuration
type DBConfig struct {
	Host     string
	Port     int
	User     string
	DBName   string
	Password string
}

func Initialize() {
	var err error
	fmt.Println("Initializing database connection...")

	DB, err = gorm.Open("mysql", DbURL(BuildDBConfig()))
	if err != nil {
		fmt.Println("Status:", err)
	}
}

func BuildDBConfig() *DBConfig {

	envPort := os.Getenv("DB_PORT")
	port, err := strconv.Atoi(envPort)

	if err != nil {
		fmt.Println(err.Error())
	}

	dbConfig := DBConfig{
		Host:     os.Getenv("DB_HOST"),
		Port:     port,
		User:     os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
		DBName:   os.Getenv("DB_NAME"),
	}

	return &dbConfig
}
func DbURL(dbConfig *DBConfig) string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
		dbConfig.User,
		dbConfig.Password,
		dbConfig.Host,
		dbConfig.Port,
		dbConfig.DBName,
	)
}
