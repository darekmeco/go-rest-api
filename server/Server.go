package server

import (
	"fmt"
	blog "github.com/cnetic.pl/restapi/app/modules/blog/models"
	user "github.com/cnetic.pl/restapi/app/modules/users/models"
	"github.com/cnetic.pl/restapi/db"
	"github.com/cnetic.pl/restapi/routes"

	"github.com/joho/godotenv"
	"log"
)

func init() {
	if err := godotenv.Load(); err != nil {
		log.Print("sad .env file found")
	} else {
		fmt.Println("We are getting the env values")
	}
}

func Run() {

	db.Initialize()
	defer db.DB.Close()
		db.DB.AutoMigrate(&user.User{}, &blog.NewsArticleCategory{}, &blog.NewsArticle{})

	r := routes.SetupRouter()
	r.Run()


}
