package types

import "github.com/dgrijalva/jwt-go"

type LoginForm struct {
  Email string `json:"email" binding:"required"`
  Password string `json:"password" binding:"required"`
}

type RegisterForm struct {
  Email string `json:"email" binding:"required"`
  Password string `json:"password" binding:"required"`
  RepeatPassword string `json:"repeat_password" binding:"required,eqfield=Password"`
}

type Claims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}
