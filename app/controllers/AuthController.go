package controllers

import (
	"fmt"
	"github.com/cnetic.pl/restapi/app/modules/users/models"
	"github.com/cnetic.pl/restapi/app/types"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	_ "github.com/swaggo/swag/example/celler/httputil"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
	"time"
)

var jwtKey = []byte("my_secret_key")

// CreateUser godoc
// @ID register-user
// @Summary Register User
// @Router /auth/register [post]
// @Accept  json
// @Produce  json
// @Param account body Struct.RegisterForm true "Add account"
// @Success 200 {object} Models.User
// @Failure 400 {object} httputil.HTTPError
func Register(c *gin.Context) {
	var register types.RegisterForm
	if err := c.ShouldBindJSON(&register); err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	var user = models.User{Password: HashAndSalt([]byte(register.Password)), Email: register.Email}
	err := models.CreateUser(&user)

	if err != nil {
		fmt.Println(err.Error())
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, user)
	}
}

func Login(c *gin.Context) {
	var login types.LoginForm

	if err := c.ShouldBindJSON(&login); err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	expirationTime := time.Now().Add(5 * time.Minute)
	claims := &types.Claims{
		Username: login.Email,
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)

	if err != nil {
		// If there is an error in creating the JWT return an internal server error
		c.JSON(http.StatusBadRequest, gin.H{"error": http.StatusInternalServerError})
		return
	}

	c.JSON(http.StatusOK, tokenString)
}

func HashAndSalt(pwd []byte) string {

    // Use GenerateFromPassword to hash & salt pwd.
    // MinCost is just an integer constant provided by the bcrypt
    // package along with DefaultCost & MaxCost.
    // The cost can be any value you want provided it isn't lower
    // than the MinCost (4)
    hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
    if err != nil {
        log.Println(err)
    }
    // GenerateFromPassword returns a byte slice so we need to
    // convert the bytes to a string and return it
    return string(hash)
}