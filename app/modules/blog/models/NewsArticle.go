package models

import (
	"time"
)

type NewsArticle struct {
	Id                    uint                  `gorm:"primary_key" json:"id"`
	Name                  string                `json:"name" binding:"required"`
	Text                  string                `json:"text" gorm:"type:text" binding:"required"`
	NewsArticleCategories []NewsArticleCategory `gorm:"many2many:news_article_news_category;association_foreignkey:id;foreignkey:id;"`
	CreatedAt             time.Time
	UpdatedAt             time.Time
}
