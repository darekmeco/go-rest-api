package models

import (
	"fmt"
	"github.com/cnetic.pl/restapi/db"
)

//GetAllNewsArticles Fetch all user data
func GetAll(users *[]NewsArticle) (err error) {
	if err = db.DB.Find(users).Error; err != nil {
		return err
	}
	return nil
}

//CreateNewsArticle ... Insert New data
func Create(user *NewsArticle) (err error) {
	if err = db.DB.Create(user).Error; err != nil {
		return err
	}

	if err = db.DB.Save(user).Error; err != nil {
		return err
	}

	return nil
}

//GetNewsArticleByID ... Fetch only one user by Id
func GetByID(user *NewsArticle, id string) (err error) {
	if err = db.DB.Where("id = ?", id).First(user).Error; err != nil {
		return err
	}
	return nil
}

func GetByEmail(user *NewsArticle, email string) (err error) {
	if err = db.DB.Where("email = ?", email).First(user).Error; err != nil {
		return err
	}
	return nil
}

//UpdateNewsArticle ... Update user
func Update(user *NewsArticle) (err error) {
	fmt.Println(user)
	db.DB.Save(user)
	return nil
}

//DeleteNewsArticle ... Delete user
func Delete(user *NewsArticle, id string) (err error) {
	db.DB.Where("id = ?", id).Delete(user)
	return nil
}
