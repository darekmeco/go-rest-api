package models

import (
	"time"
)

type NewsArticleCategory struct {
	Id           uint          `gorm:"primary_key" json:"id"`
	Name         string        `json:"name" binding:"required"`
	Text         string        `json:"text" gorm:"type:text" binding:"required"`
	NewsArticles []NewsArticle `gorm:"many2many:news_article_news_category;association_foreignkey:id;foreignkey:id"`
	CreatedAt    time.Time
	UpdatedAt    time.Time
}
