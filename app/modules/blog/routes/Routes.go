package routes

import (
	"github.com/cnetic.pl/restapi/app/modules/blog/controllers"
	"github.com/cnetic.pl/restapi/middlewares"
	"github.com/gin-gonic/gin"
)

func SetupRouter(r *gin.Engine) *gin.Engine {

	userRoutes := r.Group("/blog/news-article")
	{
		userRoutes.Use(middlewares.AuthMiddleware().MiddlewareFunc())
		{
			userRoutes.GET("", controllers.All)
			userRoutes.POST("", controllers.Create)
			userRoutes.PATCH(":id", controllers.Update)
			userRoutes.DELETE(":id", controllers.Delete)
		}
	}
	return r
}
