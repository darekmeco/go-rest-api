package controllers

import (
	"fmt"
	blog "github.com/cnetic.pl/restapi/app/modules/blog/models"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	entranslations "github.com/go-playground/validator/v10/translations/en"
	"net/http"
	"strconv"
)

var (
	statusOk = map[string]string{"status": "success", "message": "Wysłano"}
	uni      *ut.UniversalTranslator
	validate *validator.Validate
)

// CreateNewsArticle godoc
// @Summary List NewsArticles
// @Router /user [get]
// @Produce  json
// @Success 200 {array} Models.NewsArticle
func All(c *gin.Context) {
	var models []blog.NewsArticle

	err := blog.GetAll(&models)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	} else {
		c.JSON(http.StatusOK, models)
	}
}

// CreateNewsArticle godoc
// @Summary Create NewsArticle
// @Router /user/create [post]
func Create(c *gin.Context) {
	var model blog.NewsArticle
	en := en.New()
	uni := ut.New(en, en)
	trans, _ := uni.GetTranslator("en")
	validate = validator.New()
	entranslations.RegisterDefaultTranslations(validate, trans)

	validate.RegisterTranslation("required", trans, func(ut ut.Translator) error {
		return ut.Add("required", "{0} must have a value!", true) // see universal-translator for details
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T("required", fe.Field())
		return t
	})

	err:= c.ShouldBindJSON(&model)

	if err != nil {
		//errors := err.(validator.ValidationErrors)
		//c.JSON(http.StatusBadRequest, errors)
		//return
		for _, fieldErr := range err.(validator.ValidationErrors)  {
			//c.JSON(http.StatusBadRequest, t)
			c.JSON(http.StatusBadRequest, gin.H{"field": fieldErr.Field(), "error": fmt.Sprint(fieldErr)})
			return
		}
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	errr := blog.Create(&model)
	if errr != nil {
		fmt.Println(err.Error())
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, model)
	}

}

func Update(c *gin.Context) {
	var user blog.NewsArticle

	var id = c.Param("user_id")
	blog.GetByID(&user, id)

	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err := blog.Update(&user)

	if err != nil {
		fmt.Println(err.Error())
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, user)
	}
}

func Delete(c *gin.Context) {

	var user blog.NewsArticle
	var id = c.Param("user_id")
	blog.GetByID(&user, id)

	uid := strconv.Itoa(int(user.Id))
	err := blog.Delete(&user, uid)

	if err != nil {
		fmt.Println(err.Error())
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, statusOk)
	}
}




