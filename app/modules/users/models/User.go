package models

type User struct {
	Id        uint   `json:"id"`
	FirstName string `json:"first_name" binding:"required"`
	LastName  string `json:"last_name"`
	Email     string `json:"email" binding:"required"`
	Password  string `json:"-"`
	Phone     string `json:"phone" binding:"required"`
	Address   string `json:"address"`
}

func (u *User) TableName() string {
	return "user"
}
