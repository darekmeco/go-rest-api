package models

import (
	"fmt"
	"github.com/cnetic.pl/restapi/db"
)

//GetAllUsers Fetch all user data
func GetAllUsers(users *[]User) (err error) {
	if err = db.DB.Find(users).Error; err != nil {
		return err
	}
	return nil
}

//CreateUser ... Insert New data
func CreateUser(user *User) (err error) {
	if err = db.DB.Create(user).Error; err != nil {
		return err
	}
	return nil
}

//GetUserByID ... Fetch only one user by Id
func GetUserByID(user *User, id string) (err error) {
	if err = db.DB.Where("id = ?", id).First(user).Error; err != nil {
		return err
	}
	return nil
}

func GetUserByEmail(user *User, email string) (err error) {
	if err = db.DB.Where("email = ?", email).First(user).Error; err != nil {
		return err
	}
	return nil
}

//UpdateUser ... Update user
func UpdateUser(user *User) (err error) {
	fmt.Println(user)
	db.DB.Save(user)
	return nil
}

//DeleteUser ... Delete user
func DeleteUser(user *User, id string) (err error) {
	db.DB.Where("id = ?", id).Delete(user)
	return nil
}
