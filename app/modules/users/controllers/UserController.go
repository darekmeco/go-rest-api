package controllers

import (
	"fmt"
	"github.com/cnetic.pl/restapi/app/modules/users/models"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

var statusOk = map[string]string{"status": "success", "message": "Wysłano"}

// CreateUser godoc
// @Summary List Users
// @Router /user [get]
// @Produce  json
// @Success 200 {array} Models.User
func GetUsers(c *gin.Context) {
	var users []models.User

	err := models.GetAllUsers(&users)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	} else {
		c.JSON(http.StatusOK, users)
	}
}

// CreateUser godoc
// @Summary Create User
// @Router /user/create [post]
func CreateUser(c *gin.Context) {
	var user models.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err := models.CreateUser(&user)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	} else {
		c.JSON(http.StatusOK, user)
	}
}

func UpdateUser(c *gin.Context) {
	var user models.User

	var id = c.Param("user_id")
	models.GetUserByID(&user, id)

	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err := models.UpdateUser(&user)

	if err != nil {
		fmt.Println(err.Error())
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, user)
	}
}

func DeleteUser(c *gin.Context) {

	var user models.User
	var id = c.Param("user_id")
	models.GetUserByID(&user, id)

	uid := strconv.Itoa(int(user.Id))
	err := models.DeleteUser(&user, uid)

	if err != nil {
		fmt.Println(err.Error())
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, statusOk)
	}
}




